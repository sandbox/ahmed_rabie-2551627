Automatic  Refresh Views Module
------------------------
by Ahmed Rabie, ahmed.rabie90@gmail.com


Introduction	
-----------
This is  module that allows to set timer to Refresh in the views.


REQUIREMENTS
------------
This module requires the following modules:
 * Views (https://drupal.org/project/views)

Installation 
------------
 * (optional) Download and install the Automatic Refresh Views module.
 * Copy the module directory to your modules directory and activate the module.
 * to set timer,configure themodule at 'admin/config'.

CONFIGURATION
-------------
 * Configure Timer in Administration » admin » config»refresh_views»settings:


   - Use the administration pages and help (refresh views module)


   *You must name the path for view as same name of  view.
