<?php
/**
 * @file
 * Administration page callbacks for the refresh_views module.
 */

/**
 * Form builder. Configure annotations.
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function refresh_views_admin_settings() {
  $form['refresh_views_timer_view'] = array(
    '#type' => 'textfield',
    '#title' => t('Timer View'),
    '#description' => t('Enter the maximum number of timer allowed per
     view 0 for no limit).'),
    '#default_value' => variable_get('refresh_views_limit_per_node', 1),
    '#size' => 3
  );
return system_settings_form($form, TRUE); 

}

function refresh_views_admin_settings_validate($form, $form_state) {
  $timer = $form_state['values']['refresh_views_timer_view'];
	$count=db_query('SELECT COUNT(*) FROM {view_timer}')->fetchField();
	if (!is_numeric($timer)) {
    form_set_error('view_limit_per_node', t('Please enter a number.'));
    return;
  }
	else{
		if($count==0){
				db_insert('view_timer')->fields(array('timer' => $timer,))->execute();
				variable_set('refresh_views_limit_per_node', $timer);
		}
		else{
		db_update('view_timer')->fields(array('timer' => $timer,))->execute();
		variable_set('refresh_views_limit_per_node', $timer);
		}
	 }
	}
